﻿#region Imports

using Microsoft.Xna.Framework.Input;

#endregion

namespace LibXna.Content.Input
{
	public class PlayerBinding
	{
		public Buttons[] Buttons;
		public ButtonActions InputActions;
		public Keys[] Keys;
		public MouseButtons[] MouseButtons;
	}
}