﻿#region Imports

using System;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

#endregion

namespace LibXna.Content.Input
{
	public class PlayerBindingsReader : ContentTypeReader<PlayerBindings>
	{
		protected override PlayerBindings Read(ContentReader input, PlayerBindings existingInstance)
		{
			if (existingInstance != null) return existingInstance;

			// read first character {
			var c = input.Read();

			if (c != '\n')
				throw new Exception("Could not find start sentinel in character bindings");

			var builder = new StringBuilder();
			while (-1 != (c = input.Read()))
			{
				builder.Append((char) c);
			}

			var playerBindings = new PlayerBindings();
			foreach (var record in builder.ToString().Split('\n'))
			{
				var name = String.Empty;
				var playerBinding = new PlayerBinding();
				foreach (var keyValuePair in record.Split('\t').Select(field => field.Split(new[] {'='}, 2)))
				{
					switch (keyValuePair[0])
					{
						case "n":
							name = keyValuePair[1];
							break;
						case "s":
							playerBinding.InputActions = (ButtonActions) Convert.ToInt32(keyValuePair[1]);
							break;
						case "b":
							playerBinding.Buttons = ToArray<Buttons>(keyValuePair[1]);
							break;
						case "k":
							playerBinding.Keys = ToArray<Keys>(keyValuePair[1]);
							break;
						case "m":
							playerBinding.MouseButtons = ToArray<MouseButtons>(keyValuePair[1]);
							break;
					}
				}
				playerBindings[name] = playerBinding;
			}

			return playerBindings;
		}

		private static TO[] ToArray<TO>(string value)
		{
			return value == null ? null : value.Split(',').Select(b => Convert.ToInt32(b)).Cast<TO>().ToArray();
		}
	}
}