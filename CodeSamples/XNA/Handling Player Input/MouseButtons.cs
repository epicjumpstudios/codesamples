﻿namespace LibXna.Content.Input
{
	public enum MouseButtons
	{
		LeftButton,
		RightButton,
		MiddleButton,
		ScrollUp,
		ScrollDown
	}
}