#region Imports

using System.Xml.Linq;
using System.Xml.Serialization;
using LibXna.Content;
using Microsoft.Xna.Framework.Content.Pipeline;
using LibXna.Content.Input;

#endregion

namespace LibXna.ContentPipeline.Input
{
	[ContentProcessor(DisplayName = "Input Bindings Processor")]
	public class PlayerBindingsProcessor : ContentProcessor<XDocument, PlayerBindings>
	{
		public override PlayerBindings Process(XDocument input, ContentProcessorContext context)
		{
			using (var reader = input.CreateReader())
			{
				return (PlayerBindings) new XmlSerializer(typeof (PlayerBindings)).Deserialize(reader);
			}
		}
	}
}