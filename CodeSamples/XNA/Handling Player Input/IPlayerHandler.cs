﻿namespace LibXna.Main
{
	public interface IPlayerHandler<in T>
	{
		bool AllPressed(params T[] input);

		bool AllTyped(params T[] input);

		bool AnyPressed(params T[] input);

		bool AnyTyped(params T[] input);

		bool IsPressed(T input);

		bool IsTyped(T input);

		bool JustPressed(params T[] key);

		bool WasPressed(T key);

		bool WerePressed(params T[] key);
	}
}