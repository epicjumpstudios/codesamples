#region Imports

using System.IO;
using System.Linq;
using System.Text;
using LibXna.Content;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using LibXna.Content.Input;

#endregion

namespace LibXna.ContentPipeline.Input
{
	[ContentTypeWriter]
	public class PlayerBindingsWriter : ContentTypeWriter<PlayerBindings>
	{
		public override string GetRuntimeReader(TargetPlatform targetPlatform)
		{
            return "LibXna.Content.Input.PlayerBindingsReader, LibXna.Content";
		}

		protected override void Write(ContentWriter output, PlayerBindings value)
		{
			var writer = new StreamWriter(output.BaseStream, Encoding.UTF8);
			foreach (var item in value.GetEnumerable())
			{
				writer.Write("\nn={0}\ts={1}", item.Key, (int) item.Value.InputActions);

				if (item.Value.Buttons != null && item.Value.Buttons.Length > 0)
				{
					writer.Write("\tb={0}", string.Join(",", item.Value.Buttons.Cast<int>()));
				}

				if (item.Value.Keys != null && item.Value.Keys.Length > 0)
				{
					writer.Write("\tk={0}", string.Join(",", item.Value.Keys.Cast<int>()));
				}

				if (item.Value.MouseButtons != null && item.Value.MouseButtons.Length > 0)
				{
					writer.Write("&\tm={0}", string.Join(",", item.Value.MouseButtons.Cast<int>()));
				}
			}
			writer.Flush();
		}
	}
}