﻿#region Imports

using System.Linq;
using LibXna.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using LibXna.Content.Input;

#endregion

namespace LibXna.Main
{
	/// <summary>
	///     Boiler-plate class to handle Input state between update cycles.
	/// </summary>
	public sealed class PlayerInput : IPlayerHandler<Keys>, IPlayerHandler<Buttons>, IPlayerHandler<MouseButtons>
	{
		private GamePadState _gamepadCurrent, _gamepadPast;
		private KeyboardState _keyboardCurrent, _keyboardPast;
		private MouseState _mouseCurrent;
		private Vector2 _mouseDelta;
		private MouseState _mousePast;
		private Vector2 _mousePosition, _mousePositionPast;
		private int _scrollDeltaCurrent, _scrollDeltaPast;

		public Vector2 MouseDelta
		{
			get { return _mouseDelta; }
		}

		public Vector2 MousePosition
		{
			get { return _mousePosition; }
		}

		public int ScrollDelta
		{
			get { return _scrollDeltaCurrent; }
		}

		public bool ScrollMoved
		{
			get { return _scrollDeltaCurrent != 0; }
		}

		public bool AllPressed(params Buttons[] buttons)
		{
			return buttons != null && buttons.All(IsPressed);
		}

		public bool AllTyped(params Buttons[] buttons)
		{
			return buttons != null && (buttons.Any(b => AnyTyped(b)) && buttons.All(b => IsPressed(b) || WasPressed(b)));
		}

		public bool AnyPressed(params Buttons[] buttons)
		{
			return buttons != null && buttons.Any(IsPressed);
		}

		public bool AnyTyped(params Buttons[] buttons)
		{
			return buttons != null && buttons.Any(IsTyped);
		}

		public bool IsPressed(Buttons button)
		{
			return _gamepadCurrent.IsButtonDown(button);
		}

		public bool IsTyped(Buttons button)
		{
			return WasPressed(button) && !IsPressed(button);
		}

		public bool JustPressed(params Buttons[] buttons)
		{
			return AllPressed(buttons) && buttons.Any(b => !WasPressed(b));
		}

		public bool WasPressed(Buttons button)
		{
			return _gamepadPast.IsButtonDown(button);
		}

		public bool WerePressed(params Buttons[] buttons)
		{
			return buttons != null && buttons.All(WasPressed);
		}

		public bool AllPressed(params Keys[] buttons)
		{
			return buttons != null && buttons.All(IsPressed);
		}

		public bool AllTyped(params Keys[] buttons)
		{
			return buttons != null && (buttons.Any(b => AnyTyped(b)) && buttons.All(b => IsPressed(b) || WasPressed(b)));
		}

		public bool AnyPressed(params Keys[] buttons)
		{
			return buttons != null && buttons.Any(IsPressed);
		}

		public bool AnyTyped(params Keys[] buttons)
		{
			return buttons != null && buttons.Any(IsTyped);
		}

		public bool IsPressed(Keys button)
		{
			return _keyboardCurrent.IsKeyDown(button);
		}

		public bool IsTyped(Keys button)
		{
			return WasPressed(button) && !IsPressed(button);
		}

		public bool JustPressed(params Keys[] buttons)
		{
			return AllPressed(buttons) && buttons.Any(b => !WasPressed(b));
		}

		public bool WasPressed(Keys button)
		{
			return _keyboardPast.IsKeyDown(button);
		}

		public bool WerePressed(params Keys[] buttons)
		{
			return buttons != null && buttons.All(WasPressed);
		}

		public bool AllPressed(params MouseButtons[] buttons)
		{
			return buttons != null && buttons.All(IsPressed);
		}

		public bool AllTyped(params MouseButtons[] buttons)
		{
			return buttons != null && (buttons.Any(b => AnyTyped(b)) && buttons.All(b => IsPressed(b) || WasPressed(b)));
		}

		public bool AnyPressed(params MouseButtons[] buttons)
		{
			return buttons != null && buttons.Any(IsPressed);
		}

		public bool AnyTyped(params MouseButtons[] buttons)
		{
			return buttons != null && buttons.Any(IsTyped);
		}

		public bool IsPressed(MouseButtons buttons)
		{
			switch (buttons)
			{
				case MouseButtons.LeftButton:
					return _mouseCurrent.LeftButton == ButtonState.Pressed;

				case MouseButtons.MiddleButton:
					return _mouseCurrent.MiddleButton == ButtonState.Pressed;

				case MouseButtons.RightButton:
					return _mouseCurrent.RightButton == ButtonState.Pressed;

				case MouseButtons.ScrollUp:
					return _scrollDeltaCurrent > 0;

				case MouseButtons.ScrollDown:
					return _scrollDeltaCurrent < 0;
			}

			return false;
		}

		public bool IsTyped(MouseButtons button)
		{
			return WasPressed(button) && !IsPressed(button);
		}

		public bool JustPressed(params MouseButtons[] buttons)
		{
			return AllPressed(buttons) && buttons.Any(b => !WasPressed(b));
		}

		public bool WasPressed(MouseButtons button)
		{
			switch (button)
			{
				case MouseButtons.LeftButton:
					return _mousePast.LeftButton == ButtonState.Pressed;

				case MouseButtons.MiddleButton:
					return _mousePast.MiddleButton == ButtonState.Pressed;

				case MouseButtons.RightButton:
					return _mousePast.RightButton == ButtonState.Pressed;

				case MouseButtons.ScrollUp:
					return _scrollDeltaPast > 0;

				case MouseButtons.ScrollDown:
					return _scrollDeltaPast < 0;
			}

			return false;
		}

		public bool WerePressed(params MouseButtons[] buttons)
		{
			return buttons != null && buttons.All(WasPressed);
		}

		public bool CheckBinding(PlayerBinding binding)
		{
			switch (binding.InputActions)
			{
				case ButtonActions.JustPressed:
					return JustPressed(binding.Buttons) || JustPressed(binding.Keys) || JustPressed(binding.MouseButtons);

				case ButtonActions.AnyPressed:
					return AnyPressed(binding.Buttons) || AnyPressed(binding.Keys) || AnyPressed(binding.MouseButtons);

				case ButtonActions.AllPressed:
					return AllPressed(binding.Buttons) || AllPressed(binding.Keys) || AllPressed(binding.MouseButtons);

				case ButtonActions.AllTyped:
					return AllTyped(binding.Buttons) || AllTyped(binding.Keys) || AllTyped(binding.MouseButtons);

				case ButtonActions.AnyTyped:
					return AnyTyped(binding.Buttons) || AnyTyped(binding.Keys) || AnyTyped(binding.MouseButtons);
			}

			return false;
		}

		public void Update(GameTime gameTime)
		{
			_keyboardPast = _keyboardCurrent;
			_keyboardCurrent = Keyboard.GetState(PlayerIndex.One);

			_gamepadPast = _gamepadCurrent;
			_gamepadCurrent = GamePad.GetState(PlayerIndex.One);

			_mousePast = _mouseCurrent;
			_mouseCurrent = Mouse.GetState();

			_mousePositionPast = _mousePosition;
			_mousePosition = new Vector2(_mouseCurrent.X, _mouseCurrent.Y);
			_mouseDelta = _mousePosition - _mousePositionPast;

			_scrollDeltaPast = _scrollDeltaCurrent;
			_scrollDeltaCurrent = _mouseCurrent.ScrollWheelValue - _scrollDeltaPast;
		}
	}
}