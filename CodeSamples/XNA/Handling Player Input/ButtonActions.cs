﻿namespace LibXna.Content.Input
{
	public enum ButtonActions : byte
	{
		None = 0,
		JustPressed = 1,
		AnyPressed = 2,
		AllPressed = 3,
		AllTyped = 4,
		AnyTyped = 5
	}
}