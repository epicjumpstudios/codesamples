﻿#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Input;

#endregion

namespace LibXna.Content.Input
{
	public sealed class PlayerBindings : IXmlSerializable
	{
		private readonly Dictionary<string, PlayerBinding> _data = new Dictionary<string, PlayerBinding>();

		public PlayerBinding this[string @event]
		{
			get
			{
				PlayerBinding binding;
				if (!_data.TryGetValue(@event, out binding))
				{
					binding = new PlayerBinding();
					_data[@event] = binding;
				}
				return binding;
			}
			set { _data[@event] = value; }
		}

		public XmlSchema GetSchema()
		{
			return null;
		}

		public void ReadXml(XmlReader reader)
		{
			_data.Clear();

			var element = (XElement) XNode.ReadFrom(reader);
			foreach (var child in element.Elements())
			{
				var key = child.Attribute("name").Value;
				_data.Add(key, new PlayerBinding
					{
						InputActions = (ButtonActions) Enum.Parse(typeof(ButtonActions), child.Attribute("state").Value),
						Buttons = ToArray<Buttons>(child.Attribute("buttons")),
						Keys = ToArray<Keys>(child.Attribute("keys")),
						MouseButtons = ToArray<MouseButtons>(child.Attribute("mouse"))
					});
			}
		}

		public void WriteXml(XmlWriter writer)
		{
			foreach (var item in _data)
			{
				writer.WriteStartElement("Binding");
				writer.WriteAttributeString("name", item.Key);

				writer.WriteAttributeString("state", item.Value.InputActions.ToString());

				if (item.Value.Buttons != null && item.Value.Buttons.Length > 0)
					writer.WriteAttributeString("buttons", ToArrayString(" ", item.Value.Buttons));

				if (item.Value.Keys != null && item.Value.Keys.Length > 0)
					writer.WriteAttributeString("keys", ToArrayString(" ", item.Value.Keys));

				if (item.Value.MouseButtons != null && item.Value.MouseButtons.Length > 0)
					writer.WriteAttributeString("mouse", ToArrayString(" ", item.Value.MouseButtons));

				writer.WriteEndElement();
			}
		}

		public List<KeyValuePair<string, PlayerBinding>> GetEnumerable()
		{
			return _data.ToList();
		}

		public void Reset()
		{
			_data.Clear();
		}

		public static TO[] ToArray<TO>(XAttribute value)
		{
			return value == null ? null : value.Value.Split(' ').Select(v => (TO) Enum.Parse(typeof(TO), v)).ToArray();
		}

		public static string ToArrayString<TO>(string sep, ICollection<TO> array)
		{
			return array == null || array.Count < 0
				       ? string.Empty
				       : string.Join(sep, array.Select(b => b.ToString()).ToArray());
		}
	}
}