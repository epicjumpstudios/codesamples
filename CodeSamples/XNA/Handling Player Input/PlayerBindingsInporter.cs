#region Imports

using Microsoft.Xna.Framework.Content.Pipeline;
using TInput = System.Xml.Linq.XDocument;

#endregion

namespace LibXna.ContentPipeline
{
	[ContentImporter(".keys", DisplayName = "Player Bindings Importer", DefaultProcessor = "PlayerBindingsProcessor")]
	public class PlayerBindingsInporter : ContentImporter<TInput>
	{
		public override TInput Import(string filename, ContentImporterContext context)
		{
			return TInput.Load(filename);
		}
	}
}