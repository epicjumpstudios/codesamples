﻿#region Imports

using System.Collections.Generic;

#endregion

namespace LibXna.Main
{
	/// <summary>
	///     Holds Game Settings for GameApplication
	/// </summary>
	public sealed class GameConfiguration
	{
		public readonly Dictionary<string, IGameListener> Listeners = new Dictionary<string, IGameListener>();

		public string ContentPath, DefaultFont, DefaultListener;
		public int Height;
		public bool IsFullScreen, IsMaximized, IsMouseVisible, IsResizable;
		public string Keybindings;
		public float PixelsPerMeter, TargetFps;
		public string Title;
		public int Width;

		public bool MusicEnabled { get; set; }
	}
}