﻿#region Imports

using System;
using System.Windows.Forms;
using LibXna.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using LibXna.Content.Input;

#endregion

namespace LibXna.Main
{
	public sealed class GameApplication : Game
	{
		private readonly GameContext _context = new GameContext();
		private readonly IGameListener _gameListener;
		private SpriteBatch _spriteBatch;

		public GameApplication(GameConfiguration config)
		{
			if (string.IsNullOrWhiteSpace(config.DefaultFont))
			{
				throw new ApplicationException("Must specify DefaultFont in GameConfiguration");
			}

			_context.Configuration = config;
			_gameListener = config.Listeners[config.DefaultListener];

			Graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = _context.Configuration.ContentPath;
			ConvertUnits.SetDisplaySimUnitRatio(config.PixelsPerMeter);
		}

		public GraphicsDeviceManager Graphics { get; private set; }

		protected override void Draw(GameTime gameTime)
		{
			_gameListener.DrawBackground(_spriteBatch);
			_gameListener.DrawEnvironment(_spriteBatch);
			_gameListener.DrawForeground(_spriteBatch);
			_gameListener.DrawHud(_spriteBatch);
			base.Draw(gameTime);
		}

		protected override void Initialize()
		{
			IsMouseVisible = _context.Configuration.IsMouseVisible;
			TargetElapsedTime = TimeSpan.FromSeconds(1/_context.Configuration.TargetFps);

			_spriteBatch = new SpriteBatch(GraphicsDevice);

			if (!string.IsNullOrWhiteSpace(_context.Configuration.Title))
			{
				Window.Title = _context.Configuration.Title;
			}

			Graphics.PreferredBackBufferHeight = _context.Configuration.Height;
			Graphics.PreferredBackBufferWidth = _context.Configuration.Width;
			Graphics.IsFullScreen = _context.Configuration.IsFullScreen;
			Graphics.ApplyChanges();

			Window.AllowUserResizing = _context.Configuration.IsResizable;
			Window.ClientSizeChanged += Window_ClientSizeChanged;
			if (!_context.Configuration.IsFullScreen && _context.Configuration.IsMaximized)
			{
				var form = (Form) Control.FromHandle(Window.Handle);
				form.WindowState = FormWindowState.Maximized;
			}

			// set globals
			_context.Game = this;
			_context.Input = new PlayerInput();
			base.Initialize();
		}

		protected override void LoadContent()
		{
			_context.Graphics = Graphics;
			_context.Content = Content;
			_context.Camera = new Camera(Graphics.GraphicsDevice);
			_context.Font = Content.Load<SpriteFont>(_context.Configuration.DefaultFont);
			_context.PlayerBindings = string.IsNullOrWhiteSpace(_context.Configuration.Keybindings)
				                          ? new PlayerBindings()
				                          : _context.PlayerBindings =
				                            Content.Load<PlayerBindings>(_context.Configuration.Keybindings);

			_gameListener.Create(_context);
			_gameListener.ScreenResized(Window.ClientBounds.Width, Window.ClientBounds.Height);
			base.LoadContent();
		}

		protected override void UnloadContent()
		{
			_gameListener.Destroy(_context);
			base.UnloadContent();
		}

		protected override void Update(GameTime gameTime)
		{
			_context.Camera.Update(gameTime);
			_context.Input.Update(gameTime);
			_gameListener.Update(gameTime);
			base.Update(gameTime);
		}

		private void Window_ClientSizeChanged(object sender, EventArgs e)
		{
			_gameListener.ScreenResized(Window.ClientBounds.Width, Window.ClientBounds.Height);
		}
	}
}