﻿#region Imports

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace LibXna.Main
{
	public class Camera
	{
		public Vector2 PositionOffset;
		public float Rotation;
		private Vector3 _centerScreen;
		private Point _position = Point.Zero;
		private Matrix _translationMatrix;
		private float _zoom = 1f;

		public Camera(GraphicsDevice graphicsDevice)
		{
			GraphicsDevice = graphicsDevice;
		}

		public GraphicsDevice GraphicsDevice { get; private set; }

		public Point Position
		{
			get { return _position; }
			set { _position = value; }
		}

		public Matrix TranslationMatrix
		{
			get { return _translationMatrix; }
		}

		public int X
		{
			get { return _position.X; }
			set { _position.X = value; }
		}

		public int Y
		{
			get { return _position.Y; }
			set { _position.Y = value; }
		}

		public float Zoom
		{
			get { return _zoom; }

			// Negative zoom will flip image
			set { _zoom = MathHelper.Clamp(value, 0.03f, 10f); }
		}

		public Matrix GetViewMatrix(Vector2 parallax)
		{
			return Matrix.CreateTranslation(new Vector3(-new Vector2(_position.X, _position.Y)
			                                            *parallax, 0.0f))
			       *_translationMatrix;
		}

		/// <summary>
		///     Updates the camera and updates the userInput.RelativeMousePosition.  (userInput.Update() shoud be called first)
		/// </summary>
		/// <param name="gameTime"></param>
		public void Update(GameTime gameTime)
		{
			_centerScreen = new Vector3(GraphicsDevice.Viewport.Width*0.5f, GraphicsDevice.Viewport.Height*0.5f, 0f);
			var origin = new Vector3(_position.X, _position.Y, 0);
			PositionOffset = Position.ToVector2() - (new Vector2(_centerScreen.X, _centerScreen.Y)/Zoom);

			_translationMatrix = Matrix.CreateTranslation(-origin)*
			                     Matrix.CreateRotationZ(Rotation)*
			                     Matrix.CreateScale(_zoom, _zoom, 1.0f)*
			                     Matrix.CreateTranslation(_centerScreen);
		}
	}
}