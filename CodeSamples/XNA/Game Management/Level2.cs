﻿#region Imports

using System;
using Bit.Game.Actors;
using LibXna.Main;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Bit.Game
{
	public class Level2 : GameLevel
	{
		public override void Create(GameContext context)
		{
			World.Gravity = new Vector2(0f, 130.00f);
			World.ClearForces();

			Bounds = new Rectangle(0, -500, 30000, 1000);
			BackgroundLayers.Add(new DecorativeLayer
				{
					TextureModern = context.Content.Load<Texture2D>("background"),
					TextureRetro = context.Content.Load<Texture2D>("rusted metal"),
					ViewCoefficient = new Vector2(0.9f, 1),
					Bounds = Bounds,
					GameContext = context
				});

			BackgroundLayers.Add(new DecorativeLayer
				{
					TextureModern = context.Content.Load<Texture2D>("background2"),
					ViewCoefficient = new Vector2(0.85f, 1),
					Bounds = Bounds,
					GameContext = context
				});

			BackgroundLayers.Add(new DecorativeLayer
				{
					TextureModern = context.Content.Load<Texture2D>("background3"),
					ViewCoefficient = new Vector2(0.70f, 1),
					Bounds = Bounds,
					GameContext = context
				});

			var groundTexture = context.Content.Load<Texture2D>("groundchunks");
			var groundPrototype = Fixture2D.Create(World, groundTexture);

			var brickTexture = context.Content.Load<Texture2D>("metalchunk");
			var brickWidth = ConvertUnits.ToSimUnits(brickTexture.Width);
			var brickHeight = ConvertUnits.ToSimUnits(brickTexture.Height);
			var brickPrototype = Fixture2D.Create(World, brickTexture);
			brickPrototype.Body.Position = new Vector2(0, brickHeight);

            var token = Token.Create(World, GameElementsModern, context.Content);
            token.Enabled = false;

			var random = new Random();
			for (var i = 0; i < 50; i++)
			{
				var brick1 = (i == 0) ? brickPrototype : Fixture2D.CopyFrom(brickPrototype);
				var brickXOffset = random.Next((int) (brickWidth*-0.4), (int) (brickWidth*0.4));
				brick1.Body.Position = new Vector2((i*brickWidth) + brickXOffset, brickHeight);
				brick1.Body.CollisionGroup = 1;
				brick1.Body.SleepingAllowed = true;
				GameElementsRetro.Add(brick1);

				var ground1 = (i == 0) ? groundPrototype : Fixture2D.CopyFrom(groundPrototype);
				ground1.Body.Position = brick1.Body.Position;
				ground1.Body.CollisionGroup = 2;
				ground1.Body.SleepingAllowed = true;
				GameElementsModern.Add(ground1);

                for (int x = 0, y = 2; x < y; x++)
                {
                    var tokenCopy = Token.CopyFrom(token);
                    tokenCopy.Enabled = true;
                    tokenCopy.Body.Position = (brick1.Body.Position) - new Vector2(x * 25, 25);
                    tokenCopy.Body.FixtureList[0].UserData = tokenCopy;
	                tokenCopy.Body.SleepingAllowed = true;
                    GameElementsRetro.Add(tokenCopy);
                    GameElementsModern.Add(tokenCopy);
                }
			}

			context.AudioBank["Modern"] = context.Content.Load<SoundEffect>("Strut_Modern").CreateInstance();
			context.AudioBank["Retro"] = context.Content.Load<SoundEffect>("Strut_Retro").CreateInstance();
			context["LevelMode"] = LevelMode.Retro;

			base.Create(context);
		}
	}
}