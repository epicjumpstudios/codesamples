﻿#region Imports

using LibXna.Main;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace Bit.Game
{
	public class DecorativeLayer
	{
		public Rectangle Bounds = Rectangle.Empty;
		public GameContext GameContext;
		public Texture2D TextureModern, TextureRetro;
		public Vector2 ViewCoefficient = Vector2.One;

		public void Draw(SpriteBatch spriteBatch, Vector2 windowSize)
		{
			var texture = ((LevelMode) GameContext["LevelMode"]) == LevelMode.Modern ? TextureModern : TextureRetro;

			if (texture != null)
			{
				var pos = GameContext.Camera.PositionOffset*ViewCoefficient;
				spriteBatch.Draw(texture, pos, Bounds, Color.White);
			}
		}
	}
}