﻿#region Imports

using System.Collections.Generic;
using System.Linq;
using Bit.Game.Actors;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using LibXna.Main;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace Bit.Game
{
	public class GameLevel : IGameListener
	{
		protected readonly List<DecorativeLayer>
			BackgroundLayers = new List<DecorativeLayer>(),
			ForegroundLayers = new List<DecorativeLayer>();

		protected readonly List<IGameElement>
			GameElementsModern = new List<IGameElement>(),
			GameElementsRetro = new List<IGameElement>(),
			HudElements = new List<IGameElement>();

		protected Rectangle Bounds = Rectangle.Empty;

		protected Vector2 WindowSize;

		protected World World = new World(Vector2.Zero);

		private Bitman _bitman;

		private GameContext _context;

		private Token _hudToken;

		public bool Paused { get; set; }

		public bool Enabled { get; set; }

		public virtual void Create(GameContext context)
		{
			_context = context;

			// create squareman
			_bitman = new Bitman(World, GameElementsModern, context)
				{
					Box =
						{
							Position = new Vector2(0, 0),
							BodyType = BodyType.Dynamic
						}
				};

			// create token hud element
			_hudToken = Token.Create(World, GameElementsModern, context.Content);
			_hudToken.Body.Enabled = false;
			_hudToken.Scale = 0.7f;
			_hudToken.Body.Position = ConvertUnits.ToSimUnits(new Vector2(5));

			var modernAudio = context.AudioBank["Modern"];
			modernAudio.IsLooped = true;

			var retroAudio = context.AudioBank["Retro"];
			retroAudio.IsLooped = true;

			_context.PropertyChange += _context_PropertyChange;
			_context_PropertyChange("LevelMode", _context["LevelMode"]);

			modernAudio.Play();
			retroAudio.Play();

			var v = new Vertices(4)
				{
					Vector2.Zero,
					ConvertUnits.ToSimUnits(new Vector2(Bounds.Width, 0)),
					ConvertUnits.ToSimUnits(new Vector2(Bounds.Width, Bounds.Height)),
					ConvertUnits.ToSimUnits(new Vector2(0, Bounds.Height))
				};

			BodyFactory.CreateLoopShape(World, v, ConvertUnits.ToSimUnits(Bounds.X, Bounds.Y));
		}

		public virtual void Destroy(GameContext context)
		{
		}

		public void DrawBackground(SpriteBatch spriteBatch)
		{
			spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null, null,
			                  _context.Camera.TranslationMatrix);
			foreach (var l in BackgroundLayers) l.Draw(spriteBatch, WindowSize);
			spriteBatch.End();
		}

		public void DrawForeground(SpriteBatch spriteBatch)
		{
			spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null, null,
			                  _context.Camera.TranslationMatrix);
			foreach (var l in ForegroundLayers) l.Draw(spriteBatch, WindowSize);
			spriteBatch.End();
		}

		public void DrawEnvironment(SpriteBatch spriteBatch)
		{
			spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null,
			                  _context.Camera.TranslationMatrix);

			if ((LevelMode) _context["LevelMode"] == LevelMode.Modern)
			{
				foreach (var s in GameElementsModern.Where(s => s.Enabled))
					s.Draw(spriteBatch);
			}
			else
			{
				foreach (var s in GameElementsRetro.Where(s => s.Enabled))
					s.Draw(spriteBatch);
			}

			_bitman.Draw(spriteBatch);

			spriteBatch.End();
		}

		public void DrawHud(SpriteBatch spriteBatch)
		{
			spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
			foreach (var h in HudElements) h.Draw(spriteBatch);
			_hudToken.Draw(spriteBatch);
			spriteBatch.DrawString(_context.Font, string.Format("{0}", _bitman.Tokens),
			                       ConvertUnits.ToDisplayUnits(_hudToken.Body.Position) + new Vector2(30, 0), Color.White);

			if (Paused)
			{
				var middleScreen = (WindowSize - _context.Font.MeasureString("PAUSED"))*0.5f;
				Draw2.Fill(spriteBatch, new Rectangle(0, 0, (int) WindowSize.X, (int) WindowSize.Y),
				           new Color(20, 20, 20, 120));
				spriteBatch.DrawString(_context.Font, "PAUSED", middleScreen, Color.White);
			}
			spriteBatch.End();
		}

		public void ScreenResized(int width, int height)
		{
			WindowSize = new Vector2(width, height);

			if (Bounds != Rectangle.Empty)
			{
				_context.Camera.Zoom = height/(float) Bounds.Height;
			}
		}

		public void Update(GameTime gameTime)
		{
			var timeLapse = (float) gameTime.ElapsedGameTime.TotalMilliseconds*0.001f;
			_hudToken.Update(gameTime);

			var isPausePressed = _context.CheckBinding("pause");
			Paused = (Paused) ? !isPausePressed : isPausePressed;

			var modernAudio = _context.AudioBank["Modern"];
			var retroAudio = _context.AudioBank["Retro"];

			if (_context.Configuration.MusicEnabled)
			{
				if (Paused)
				{
					if (modernAudio.State == SoundState.Playing)
					{
						modernAudio.Pause();
					}
					if (retroAudio.State == SoundState.Playing)
					{
						retroAudio.Pause();
					}
				}
				else
				{
					if (modernAudio.State == SoundState.Stopped)
					{
						modernAudio.Play();
					}
					else if (retroAudio.State == SoundState.Paused)
					{
						modernAudio.Resume();
					}
					if (retroAudio.State == SoundState.Stopped)
					{
						retroAudio.Play();
					}
					else if (retroAudio.State == SoundState.Paused)
					{
						retroAudio.Resume();
					}
				}
			}
			else
			{
				modernAudio.Stop();
				retroAudio.Stop();
			}

			if (Paused)
			{
				return;
			}

			if (_context.CheckBinding("quit"))
			{
				_context.Game.Exit();
				return;
			}

			if (_context.CheckBinding("bitflip"))
			{
				_context["LevelMode"] = ((LevelMode) _context["LevelMode"] == LevelMode.Modern) ? LevelMode.Retro : LevelMode.Modern;
			}

			_bitman.Update(gameTime);

			var levelMode = (LevelMode) _context["LevelMode"];
			foreach (var e in GameElementsModern)
			{
				if (levelMode == LevelMode.Modern)
				{
					e.Update(gameTime);
				}
				e.Enabled = levelMode == LevelMode.Modern;
			}

			foreach (var e in GameElementsRetro)
			{
				if (levelMode == LevelMode.Retro)
				{
					e.Update(gameTime);
				}
				e.Enabled = levelMode == LevelMode.Retro;
			}

			var position = ConvertUnits.ToDisplayUnits(_bitman.Box.Position);

			// unbounded _world
			if (Bounds == Rectangle.Empty)
			{
				_context.Camera.Position = new Point((int) position.X, (int) position.Y);
			}

				// bounded _world
			else
			{
				var centerScreen = (WindowSize/_context.Camera.Zoom)*0.5f;

				var leftCameraBound = Bounds.X + centerScreen.X;
				var rightCameraBound = Bounds.Width + Bounds.X - centerScreen.X;
				var topCameraBound = Bounds.Y + centerScreen.Y;
				var bottomCameraBounds = Bounds.Height + Bounds.Y - centerScreen.Y;

				_context.Camera.Position = new Point(
					(int) MathHelper.Clamp(position.X, leftCameraBound, rightCameraBound),
					(int) MathHelper.Clamp(position.Y, topCameraBound, bottomCameraBounds));
			}

			foreach (var e in HudElements) e.Update(gameTime);
			World.Step(timeLapse);
		}

		private void _context_PropertyChange(string arg1, object value)
		{
			if (arg1 == "LevelMode")
			{
				_context.AudioBank["Modern"].Volume = ((LevelMode) value == LevelMode.Modern) ? 1f : 0f;
				_context.AudioBank["Retro"].Volume = ((LevelMode) value == LevelMode.Retro) ? 1f : 0f;
			}
		}
	}
}