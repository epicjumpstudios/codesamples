﻿#region Imports

using Microsoft.Xna.Framework.Graphics;

#endregion

namespace LibXna.Main
{
	/// <summary>
	/// </summary>
	public interface IGameListener : IUpdatable
	{
		void Create(GameContext context);

		void Destroy(GameContext context);

		void DrawBackground(SpriteBatch spriteBatch);

		void DrawForeground(SpriteBatch spriteBatch);

		void DrawEnvironment(SpriteBatch spriteBatch);

		void DrawHud(SpriteBatch spriteBatch);

		void ScreenResized(int width, int height);
	}
}