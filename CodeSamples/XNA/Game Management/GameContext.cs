﻿#region Imports

using System;
using System.Collections.Generic;
using LibXna.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using LibXna.Content.Input;

#endregion

namespace LibXna.Main
{
	public sealed class GameContext
	{
		public readonly Dictionary<string, SoundEffectInstance> AudioBank = new Dictionary<string, SoundEffectInstance>();
		private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();

		public Camera Camera;
		public GameConfiguration Configuration;
		public ContentManager Content;
		public SpriteFont Font;
		public Game Game;
		public GraphicsDeviceManager Graphics;
		public PlayerInput Input;
		public PlayerBindings PlayerBindings;

		public object this[string key]
		{
			get { return _properties[key]; }
			set
			{
				_properties[key] = value;

				if (PropertyChange != null)
					PropertyChange(key, value);
			}
		}

		public event Action<string, object> PropertyChange;

		public bool CheckBinding(string bindingName)
		{
			return bindingName != null && Input.CheckBinding(PlayerBindings[bindingName]);
		}
	}
}