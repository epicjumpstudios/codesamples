﻿#region Imports

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace LibXna.Main.Render
{
	public class TextureRenderer : IRenderDecoration
	{
		public TextureRenderer()
		{
			Enabled = true;
		}

		public Texture2D Texture { get; set; }

		public bool Enabled { get; set; }

		public void Draw(SpriteBatch spriteBatch, Color color, Vector2 position, Vector2 origin, float rotation, float scale,
		                 SpriteEffects spriteEffect)
		{
			if (!Enabled) return;

			spriteBatch.Draw(Texture, position, null, color, rotation, origin, scale, spriteEffect, 0f);
		}

		public static implicit operator TextureRenderer(Texture2D r)
		{
			return new TextureRenderer {Texture = r};
		}
	}
}