﻿#region Imports

using LibXna.Main;
using LibXna.Main.Machines;
using LibXna.Main.Render;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace Bit.Game.Actors
{
	public sealed class Eyes : IUpdatable, IRenderDecoration
	{
		public const State Blinking = (State) 1;

		private readonly FrameRenderer<IRenderDecoration> _blinkRenderer = new FrameRenderer<IRenderDecoration>();
		private readonly StateRenderer<TextureRenderer> _stateRenderer = new StateRenderer<TextureRenderer>();

		private bool _isEnabled = true;

		public Eyes()
		{
			_blinkRenderer.Register(State.Default, 200, _stateRenderer);
		}

		public void Draw(SpriteBatch spriteBatch, Color color, Vector2 position, Vector2 origin, float rotation, float scale,
		                 SpriteEffects spriteEffect)
		{
			_blinkRenderer.Draw(spriteBatch, color, position, origin, rotation, scale, spriteEffect);
		}

		public bool Enabled
		{
			get { return _isEnabled; }
			set
			{
				_isEnabled = value;
				_blinkRenderer.Enabled = value;
				_stateRenderer.Enabled = value;
			}
		}

		public void Update(GameTime gameTime)
		{
			_blinkRenderer.Update(gameTime);
		}

		public void Register(State state, Texture2D texture)
		{
			_stateRenderer.Register(state, texture);

			if (state == Blinking)
			{
				_blinkRenderer.Register(Blinking, 4800, (TextureRenderer) texture);
			}
		}
	}
}