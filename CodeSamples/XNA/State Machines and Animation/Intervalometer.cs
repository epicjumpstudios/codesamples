﻿#region Imports

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

#endregion

namespace LibXna.Main.Machines
{
	public class Intervalometer : IUpdatable, IDisposable
	{
		public delegate void Notifier(long interval, int occurrences);

		private readonly List<Item> _items = new List<Item>();
		private bool _enabled = true;

		public void Dispose()
		{
			_items.Clear();
		}

		public bool Enabled
		{
			get { return _enabled; }
			set { _enabled = value; }
		}

		public void Update(GameTime gameTime)
		{
			if (!Enabled) return;

			var elapsedMs = (long) gameTime.ElapsedGameTime.TotalMilliseconds;

			foreach (var t in _items)
			{
				t.Remaining -= elapsedMs;

				if (t.Remaining > 0)
				{
					continue;
				}

				if (t.Remaining == 0)
				{
					t.Remaining = t.Interval;
					t.Handler(t.Interval, 1);
					continue;
				}

				var occurences = (int) Math.Floor(-t.Remaining/(float) t.Interval);
				t.Remaining = (t.Remaining%t.Interval) + t.Interval;
				t.Handler(t.Interval, 1 + occurences);
			}
		}

		public void Register(long interval, Notifier handler)
		{
			_items.Add(new Item {Interval = interval, Remaining = interval, Handler = handler});
		}

		internal class Item
		{
			internal Notifier Handler;
			internal long Interval;
			internal long Remaining;
		}
	}
}