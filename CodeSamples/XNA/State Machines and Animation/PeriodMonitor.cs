﻿#region Imports

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

#endregion

namespace LibXna.Main.Machines
{
	public class PeriodMonitor : IUpdatable, IDisposable
	{
		public delegate void Notifier(State state, bool isRunningSlow);

		private readonly List<Item> _items = new List<Item>();

		private bool _enabled = true;
		private int _nextState;
		private long _surplus;

		public void Dispose()
		{
			_items.Clear();
		}

		public bool Enabled
		{
			get { return _enabled; }
			set { _enabled = value; }
		}

		public void Update(GameTime gameTime)
		{
			if (!Enabled || _items.Count == 0) return;

			var budget = (long) gameTime.ElapsedGameTime.TotalMilliseconds + _surplus;
			var isSlow = gameTime.IsRunningSlowly;

			_surplus = 0;

			var itemLength = _items.Count;
			var lastItem = itemLength - 1;
			while (_nextState < itemLength)
			{
				var item = _items[_nextState];
				var diff = budget - item.Period;

				if (diff < 0)
				{
					_surplus = budget;
					break;
				}

				budget = diff;
				item.Handler(item.State, isSlow);

				// reset loop if last item
				_nextState = (_nextState == lastItem) ? 0 : _nextState + 1;
			}
		}

		public void Register(State state, long period, Notifier handler)
		{
			_items.Add(new Item {State = state, Period = period, Handler = handler});
		}

		internal class Item
		{
			internal Notifier Handler;
			internal long Period;
			internal State State;
		}
	}
}