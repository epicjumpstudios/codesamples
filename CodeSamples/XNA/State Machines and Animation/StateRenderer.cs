﻿#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using LibXna.Main.Machines;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace LibXna.Main.Render
{
	public class StateRenderer<T> : IRenderDecoration
		where T : class, IRenderDecoration
	{
		private readonly List<Tuple<State, T>> _states = new List<Tuple<State, T>>();

		private T _currentRenderable;
		private State _currentState = State.Default;

		public StateRenderer()
		{
			Enabled = true;
		}

		public State CurrentState
		{
			get { return _currentState; }
			set
			{
				foreach (var state in _states.Where(state => state.Item1 == value))
				{
					_currentState = value;
					_currentRenderable = state.Item2;
					return;
				}
			}
		}

		public bool Enabled { get; set; }

		public void Draw(SpriteBatch spriteBatch, Color color, Vector2 position, Vector2 origin, float rotation, float scale,
		                 SpriteEffects spriteEffect)
		{
			if (_currentRenderable == null || !Enabled)
				return;

			_currentRenderable.Draw(spriteBatch, color, position, origin, rotation, scale, spriteEffect);
		}

		public void Register(State state, T texture)
		{
			_states.Add(Tuple.Create(state, texture));

			if (state == State.Default)
			{
				CurrentState = State.Default;
			}
		}
	}
}