﻿#region Imports

using LibXna.Main.Machines;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace LibXna.Main.Render
{
	public class FrameRenderer<T> : IUpdatable, IRenderDecoration
		where T : class, IRenderDecoration
	{
		public const State Frame0 = 0;
		public const State Frame1 = (State) 1;
		public const State Frame2 = (State) 2;
		public const State Frame3 = (State) 3;
		public const State Frame4 = (State) 4;
		public const State Frame5 = (State) 5;
		public const State Frame6 = (State) 6;

		private readonly PeriodMonitor _periodMonitor = new PeriodMonitor();
		private readonly StateRenderer<T> _stateRenderer = new StateRenderer<T>();

		private bool _enabled = true;

		public void Draw(SpriteBatch spriteBatch, Color color, Vector2 position, Vector2 origin, float rotation, float scale,
		                 SpriteEffects spriteEffect)
		{
			if (!Enabled) return;

			_stateRenderer.Draw(spriteBatch, color, position, origin, rotation, scale, spriteEffect);
		}

		public bool Enabled
		{
			get { return _enabled; }
			set { _enabled = value; }
		}

		public void Update(GameTime gameTime)
		{
			if (!Enabled) return;

			_periodMonitor.Update(gameTime);
		}

		public void Register(State state, long period, T t)
		{
			_stateRenderer.Register(state, t);
			_periodMonitor.Register(state, period, (s, b) => _stateRenderer.CurrentState = s);
		}
	}
}