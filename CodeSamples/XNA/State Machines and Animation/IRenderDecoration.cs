﻿#region Imports

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace LibXna.Main
{
	public interface IRenderDecoration
	{
		bool Enabled { get; set; }

		void Draw(SpriteBatch spriteBatch, Color color, Vector2 position, Vector2 origin, float rotation, float scale,
		          SpriteEffects spriteEffect);
	}
}