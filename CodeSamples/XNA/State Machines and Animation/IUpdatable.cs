﻿#region Imports

using Microsoft.Xna.Framework;

#endregion

namespace LibXna.Main
{
	public interface IUpdatable
	{
		bool Enabled { get; set; }

		void Update(GameTime gameTime);
	}
}