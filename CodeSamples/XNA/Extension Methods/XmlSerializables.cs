﻿#region Imports

using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

#endregion

namespace LibXna.Common
{
	public static class XmlSerializables
	{
		public static T FromFile<T>(string path)
		{
			return (T) new XmlSerializer(typeof (T)).Deserialize(File.OpenRead(path));
		}

		public static bool ToFile<T>(this T t, string path)
		{
			using (var stream = File.OpenWrite(path))
			{
				new XmlSerializer(typeof (T)).Serialize(stream, t);
			}

			return File.Exists(path);
		}

		public static T ToObject<T>(this XElement element)
		{
			using (var reader = element.CreateReader())
			{
				return (T) new XmlSerializer(typeof (T)).Deserialize(reader);
			}
		}

		public static void WriteTo<T>(this T t, XmlWriter writer)
		{
			new XmlSerializer(typeof (T)).Serialize(writer, t);
		}
	}
}