﻿namespace LibXna.Common
{
	public static class Numbers
	{
		public static int Clamp(this int value, int min, int max)
		{
			return (value < min) ? min : (value > max) ? max : value;
		}

		public static bool InRange(this int value, int min, int max)
		{
			return value >= min && value <= max;
		}

		public static bool InRange(this long value, long min, long max)
		{
			return value >= min && value <= max;
		}

		public static bool InRange(this float value, float min, float max)
		{
			return value >= min && value <= max;
		}

		public static bool InRange(this double value, double min, double max)
		{
			return value >= min && value <= max;
		}
	}
}